## POGO Acoustic Machine Learning Workshop

Collection of Python notebooks on acoustic ML for the POGO Acoustic Machine Learning Workshop on 
May 20-22, at the VLIZ InnovOcean Center in Oostend, Belgium.

- [Danelle Cline](dcline@mbari.org), Senior Software Engineer, Monterey Bay Aquarium Research Institute (MBARI)
- [John Ryan](ryjo@mbari.org), Senior Research Specialist, Monterey Bay Aquarium Research Institute (MBARI)
 
### Acoustic Workshop Description

Passive acoustic sensing in the ocean provides a wealth of information about the presence and activity of 
marine life, as well as anthropogenic noise that can negatively impact marine life. This mode of sensing 
generates very large and complex data sets.  In this research domain, ML is proving to be highly effective. 
This tutorial will cover ML fundamentals, optimum decimation filtering, spectrogram enhancement methods, 
and classification using convolutional neural networks (CNN).  We will focus on end-to-end analysis methods
for one type of sound source: low frequency whale calls, including detection and classification. 
Some basic experience in Python programming in Jupyter notebooks would be beneficial. 
Data will be provided for the tutorial. If you wish to bring your own data for experimenting 
outside of class time, please contact us to understand required dataset organization.

## Install

This can either be run locally or in an AWS instance.  To run in AWS you will need to setup an account 
at https://aws.amazon.com/ and provide your credit card for billing.

## Data

Data used in this notebook is freely available at s3://pogoacousticworkshop2019/pogodata.tar.gz

#### First, checkout the code

```bash
git clone https://bitbucket.org/mbari/pogo-acoustic-workshop2019.git
```

#### AWS cloud setup

The workshop uses the AWS cloud service as it provides the graphical processing units GPUS needed for some of the lessons
but not all.  

The cost for this depends on the type of Amazon Machine Instance AMI you choose from the
cloud formation template. We recommended for experimenting that you use the default: ml.p2.xlarge which contains 
1 GPU, 4 vCPUs, 61 GB RAM. As of 10-09-2020 the ml.p2.xlarge costs $0.900 USD per hour.

----
Be sure to SHUTDOWN your instance when you are not using it to avoid payment. It can be shutdown and resumed at anytime 
without losing your work.  Be sure to save your notebooks before deleting it, however.
----

To launch in AWS:

1. Login to your AWS account
    https://console.aws.amazon.com/console/home
    ![ Image link ](/doc/img/flow.jpg)

2. Open the AWS CloudFormation console at https://console.aws.amazon.com/cloudformation
    - Click *Create Stack* With existing resources (import resources)
    - Click Next
    - In Specify template, select Upload a template file and use the Amazon S3 URL:
    ```s3://pogoacousticworkshop2019/aws_sagemaker_notebook.json```
    ![ Image link ](/doc/img/aws_template.jpg)
 
#### Local setup

The code in this repository can be run outside of the AWS environment on a CPU. Some lessons
will be much slower to run but this is okay for experimenting.

The notebooks in this could also be modified to run in a Google Colab environment as well 
with some modifications such as installing the dependencies specified in Dockerfile. This is 
untested and not guaranteed to work.

Docker is a *container* system that allows for execution on any platform: Mac OSX, Linux or Windows.

To run locally in a Docker container:
 
1. Install docker [Docker](https://docs.docker.com/get-docker/)

2. Build the docker container with the following command in a terminal window. This should be executed in the same directory the code was check-out to.
```
docker build -t pogo-acoustic-workshop2019 .
```

Run the container

- it run interactively
- rm remove when done
- (optional) map your data volume in /home/lucille/data to the container volume /data

* without mapping the notebooks - all notebooks will be lost when you exit or kill the container.
 
Run, overriding the example notebooks in this repository:
```
docker run -it --rm -v $PWD:/home/jovyan/work/ pogo-acoustic-workshop2019
```

Open the notebooks in your web browser at [http://localhost:8888/](http://localhost:8888/).
Your token should be printed in the output of the docker run command.  Enter that in the browser to open
the notebook.  Run through each lesson in succession.


 
