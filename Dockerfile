FROM jupyter/base-notebook

MAINTAINER Danelle Cline <dcline@mbari.org>

USER root
RUN apt-get -qq update && \
    apt-get install -y libsndfile1 && \
    apt-get install -y gcc && \
    apt-get install -y libsm6 && \
    apt-get install -y libatlas-base-dev && \
    apt-get install -y libxrender-dev && \
    apt-get install -y libxext-dev && \
    apt-get install -y python3-tables

# Uncomment to include PDF support (Warning - this makes the image much larger!)
# apt-get install -y texlive-xetex pandoc

USER $NB_UID

RUN conda create --name pogo-acoustic-workshop2019
RUN /bin/bash -c "source activate pogo-acoustic-workshop2019"

# Install Python 3 packaes
RUN conda install --quiet --yes \
    'cython'

# A few are not in the conda channels, so install with pip
ADD requirements.txt .
RUN pip install -r requirements.txt

# Add in the sample data
WORKDIR /home/jovyan/work/call_samples
ADD call_samples .
WORKDIR /home/jovyan/work/lesson1_data_exploration
ADD lesson1_data_exploration .
WORKDIR /home/jovyan/work/lesson2_spectrogram_generation
ADD lesson2_spectrogram_generation .
WORKDIR /home/jovyan/work/lesson3_transfer_learning_classification
ADD lesson3_transfer_learning_classification .
WORKDIR /home/jovyan/work/lesson4_decimation_filtering
ADD lesson4_decimation_filtering .
WORKDIR /home/jovyan/work